#!/bin/bash

../../bin/mockgen -destination=./mocks/mock_inputprocessor.go -package=mocks raveena/input IProcessor
../../bin/mockgen -destination=./mocks/mock_command.go -package=mocks raveena/command IGpioCommand
../../bin/mockgen -destination=./mocks/mock_controller.go -package=mocks raveena/controller IController
../../bin/mockgen -destination=./mocks/mock_pinwrapper.go -package=mocks raveena/wrapper IPinWrapper