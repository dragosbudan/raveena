package tests

import (
	"raveena/controller"
	"raveena/input"
	"raveena/mocks"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
)

func Test_should_call_controller_functions_on_each_controller(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockController1 := mocks.NewMockIController(mockCtrl)
	mockController2 := mocks.NewMockIController(mockCtrl)

	sut := input.NewInputProcessor([]controller.IController{mockController1, mockController2})
	mockController1.EXPECT().ChangeDuty(1).Times(1)
	mockController1.EXPECT().GetType().Return(controller.SERVO)
	mockController1.EXPECT().ChangeState(controller.RUNNING).Times(1)
	mockController2.EXPECT().ChangeDuty(1).Times(1)
	mockController2.EXPECT().GetType().Return(controller.DC)
	mockController2.EXPECT().ChangeState(controller.RUNNING).Times(1)
	err := sut.Process(&input.Data{Servo: 1, DcMotor: 1, State: controller.RUNNING})

	if err != nil {
		t.Errorf("Error was %v, expected %v", err, nil)
	}
}

func Test_should_return_error_when_data_is_nil(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockController1 := mocks.NewMockIController(mockCtrl)
	mockController2 := mocks.NewMockIController(mockCtrl)

	sut := input.NewInputProcessor([]controller.IController{mockController1, mockController2})
	err := sut.Process(nil)

	if !strings.Contains(err.Error(), "Input data is nil") {
		t.Errorf("Error was %v, expected %v", err, "Input data is nil")
	}
}
