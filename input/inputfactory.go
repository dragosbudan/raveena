package input

import (
	"raveena/controller"
)

// NewKeyInputFactory :
func NewKeyInputFactory(smc []controller.IController) *KeyInput {
	i := &KeyInput{
		InputHandler: NewKeyInputHandler(NewInputProcessor(smc)),
	}
	return i
}

// NewGamepadInputFactory :
func NewGamepadInputFactory(smc []controller.IController) *GamepadInput {
	i := &GamepadInput{
		InputHandler: NewGamepadInputHandler(NewInputProcessor(smc)),
	}

	i.Init()

	return i
}
