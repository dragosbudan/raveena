package command

// IGpioCommand :
type IGpioCommand interface {
	SwitchAllLow()
	TurnCW(duty int)
	TurnCCW(duty int)
	WaveDrive()
	PrintAllPins()
}
