package input

import (
	"fmt"
	"math"
	"raveena/controller"

	"github.com/splace/joysticks"
)

// GamepadInput :
// Xbox360 controller:
// Joy 1 (X,Y): device.OnMove(1)
// Joy 2 (-1, X): device.OnMove(2)
// Joy 2 (Y, -1): device.OnMove(3)
// D-pad: device.OnMove(4)
type GamepadInput struct {
	InputHandler *GamepadHandler
	b1press      chan joysticks.Event
	b2press      chan joysticks.Event
	startpress   chan joysticks.Event
	selectpress  chan joysticks.Event
	h1move       chan joysticks.Event
	h2moveX      chan joysticks.Event
	done         chan bool
	device       *joysticks.HID
}

// Init :
func (gi *GamepadInput) Init() {
	device := joysticks.Connect(1)

	if device == nil {
		panic("no HIDs")
	}
	fmt.Printf("HID#1:- Buttons:%d, Hats:%d\n", len(device.Buttons), len(device.HatAxes)/2)

	// make channels for specific events
	gi.b1press = device.OnClose(9)
	gi.b2press = device.OnClose(10)
	gi.startpress = device.OnClose(8)
	gi.selectpress = device.OnClose(7)
	gi.h1move = device.OnMove(1)
	gi.h2moveX = device.OnMove(2)
	gi.done = make(chan bool)

	// feed OS events onto the event channels.
	go device.ParcelOutEvents()

	go gi.InputHandler.Consume()
}

// WaitForInput :
func (gi *GamepadInput) WaitForInput() {
	inputProcessorData := Data{
		State: controller.RUNNING,
	}
	// handle event channels
	go func() {
		for {
			select {
			case <-gi.b1press:
				fmt.Println("button #1 pressed")
			case <-gi.b2press:
				fmt.Println("button #2 pressed")
			case h1 := <-gi.h1move:
				hpos := h1.(joysticks.CoordsEvent)
				if math.Abs(float64(hpos.X)) <= 0.20 {
					hpos.X = 0
				}
				if math.Abs(float64(hpos.Y)) <= 0.20 {
					hpos.Y = 0
				}
				//fmt.Println("hat #1 moved too:", hpos.X, hpos.Y)
				inputProcessorData.DcMotor = int(hpos.Y * 32)

			case h2 := <-gi.h2moveX:
				hpos := h2.(joysticks.CoordsEvent)
				if math.Abs(float64(hpos.X)) == -1 {
					hpos.X = 0
				}
				if math.Abs(float64(hpos.Y)) <= 0.40 {
					hpos.Y = 0
				}
				//fmt.Println("hat #2 moved too:", hpos.X, hpos.Y)
				inputProcessorData.Servo = int(hpos.Y * 45)

			case <-gi.startpress:
				inputProcessorData.State = controller.STOPPED
				gi.done <- true
			}

			//fmt.Println(inputProcessorData)
			gi.InputHandler.Scan(inputProcessorData)
		}
	}()
	<-gi.done
}

// NewGamepadInput :
func NewGamepadInput(gih *GamepadHandler) *GamepadInput {
	gi := &GamepadInput{
		InputHandler: gih,
	}

	gi.Init()

	return gi
}
