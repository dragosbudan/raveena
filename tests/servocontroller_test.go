package tests

import (
	"raveena/controller"
	"raveena/mocks"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
)

func Test_should_stop_sc_when_state_is_changed_to_STOPPED(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().WaveDrive().AnyTimes()
	mockCommand.EXPECT().SwitchAllLow().Times(1)
	sut := controller.NewServoController(controller.RUNNING, mockCommand)
	go sut.Run()
	time.Sleep(2 * time.Second)
	sut.ChangeState(controller.STOPPED)
	time.Sleep(1 * time.Second)

	if sut.State != controller.STOPPED {
		t.Errorf("sc.Stopped: expected %v, was %v", controller.STOPPED, sut.State)
	}
}

func Test_should_pause_sc_when_state_is_changed_to_PAUSED(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().WaveDrive().MinTimes(2)
	mockCommand.EXPECT().SwitchAllLow().MinTimes(2)
	sut := controller.NewServoController(controller.RUNNING, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeState(controller.PAUSED)
	time.Sleep(1 * time.Second)

	if sut.State != controller.PAUSED {
		t.Errorf("sc.Stopped: expected %v, was %v", controller.PAUSED, sut.State)
	}
}

func Test_sc_should_not_run_again_after_STOPPED(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().SwitchAllLow().Times(1)
	sut := controller.NewServoController(controller.STOPPED, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeState(controller.RUNNING)
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(1)
	time.Sleep(1 * time.Second)

	if sut.State != controller.RUNNING {
		t.Errorf("Dc.Stopped: expected %v, was %v", controller.RUNNING, sut.State)
	}
}

func Test_sc_should_run_again_after_PAUSED(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().SwitchAllLow().MinTimes(2)
	mockCommand.EXPECT().TurnCW(gomock.Any()).MinTimes(2)
	sut := controller.NewDcController(controller.PAUSED, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeState(controller.RUNNING)
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(1)
	time.Sleep(1 * time.Second)

	if sut.State != controller.RUNNING {
		t.Errorf("Dc.Stopped: expected %v, was %v", controller.RUNNING, sut.State)
	}
}

func Test_should_sc_turn_ccw_when_duty_is_negative(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().WaveDrive().MinTimes(1)
	mockCommand.EXPECT().SwitchAllLow().MinTimes(1)
	mockCommand.EXPECT().TurnCCW(-1).MinTimes(1)
	sut := controller.NewServoController(controller.RUNNING, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(-1)
	time.Sleep(1 * time.Second)

	sut.ChangeState(controller.STOPPED)
	time.Sleep(1 * time.Second)
}

func Test_should_sc_turn_cw_when_duty_is_positive(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().WaveDrive().MinTimes(1)
	mockCommand.EXPECT().TurnCW(1).MinTimes(1)
	sut := controller.NewServoController(controller.RUNNING, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(1)
	time.Sleep(1 * time.Second)
}

func Test_should_sc_swave_drive_duty_is_0(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockCommand := mocks.NewMockIGpioCommand(mockCtrl)
	mockCommand.EXPECT().WaveDrive().MinTimes(2)
	mockCommand.EXPECT().TurnCW(1).MinTimes(1)
	sut := controller.NewServoController(controller.RUNNING, mockCommand)
	go sut.Run()
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(1)
	time.Sleep(1 * time.Second)
	sut.ChangeDuty(0)
	time.Sleep(1 * time.Second)
}
