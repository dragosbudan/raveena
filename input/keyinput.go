package input

import (
	"bufio"
	"os"
	"raveena/controller"
)

// KeyInput : system keyboard input
type KeyInput struct {
	InputHandler IHandler
}

// WaitForInput :
func (si *KeyInput) WaitForInput() {
	go si.produce()
	go si.consume()
	<-done1
}

var done1 = make(chan bool)

func (si *KeyInput) produce() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		si.InputHandler.Scan(Data{Servo: 0, DcMotor: 0, State: controller.PAUSED})
	}
	done1 <- true
}

func (si *KeyInput) consume() {
	si.InputHandler.Consume()
}
