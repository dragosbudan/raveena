package command

import (
	"fmt"
	"math"
	"time"

	rpio "github.com/stianeikeland/go-rpio"
)

// DcCommand :
type DcCommand struct {
	Pins        []rpio.Pin
	CurrentDuty int
}

// PrintAllPins :
func (dc *DcCommand) PrintAllPins() {
	for i, pin := range dc.Pins {
		fmt.Printf("Pin(%v): %v\n", i, pin.Read())
	}

}

// SwitchAllLow :
func (dc *DcCommand) SwitchAllLow() {
	for i, pin := range dc.Pins {
		if i != 3 {
			pin.Write(rpio.Low)
		}
	}
}

// TurnCW :
func (dc *DcCommand) TurnCW(duty int) {
	dc.Pins[0].DutyCycle(uint32(duty), 32)
	dc.Pins[1].Write(rpio.High)
	dc.Pins[2].Write(rpio.Low)
	time.Sleep(time.Second / 32)
	dc.PrintAllPins()
}

// TurnCCW :
func (dc *DcCommand) TurnCCW(duty int) {
	realDuty := math.Abs(float64(duty))
	dc.Pins[0].DutyCycle(uint32(realDuty), 32)
	dc.Pins[1].Write(rpio.Low)
	dc.Pins[2].Write(rpio.High)
	time.Sleep(time.Second / 32)
	dc.PrintAllPins()
}

// WaveDrive :
func (dc *DcCommand) WaveDrive() {
	dc.Pins[3].Write(rpio.High)
}

// FullStep :
func (dc *DcCommand) FullStep() {
	dc.SwitchAllLow()
}

// NewDcCommand :
func NewDcCommand(pinNumbers []int) *DcCommand {
	var pins []rpio.Pin
	fmt.Println("Pins", pinNumbers)

	for i := 0; i < len(pinNumbers); i++ {
		pin := rpio.Pin(pinNumbers[i])
		if i == 0 {
			pin.Mode(rpio.Pwm)
			pin.Freq(64000)
			pin.DutyCycle(0, 32)
		} else {
			pin.Mode(rpio.Output)
		}

		fmt.Println("Appending pin", pin)
		pins = append(pins, pin)
	}

	command := &DcCommand{
		Pins:        pins,
		CurrentDuty: 0,
	}

	command.Pins[3].Write(rpio.High)

	return command
}
