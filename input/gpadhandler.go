package input

// GamepadHandler :
type GamepadHandler struct {
	Data           chan Data
	InputProcessor IProcessor
}

// Scan :
func (gih *GamepadHandler) Scan(jc Data) {
	if gih.Data != nil {
		gih.Data <- jc
	}

}

// Consume :
func (gih *GamepadHandler) Consume() {
	if gih.Data != nil {
		for {
			msg := <-gih.Data
			gih.InputProcessor.Process(&msg)
		}
	}
}

// NewGamepadInputHandler :
func NewGamepadInputHandler(ip IProcessor) *GamepadHandler {
	gih := &GamepadHandler{
		InputProcessor: ip,
		Data:           make(chan Data),
	}

	return gih
}
