package command

import (
	"fmt"
	"math"
	"time"

	rpio "github.com/stianeikeland/go-rpio"
)

// ServoCommand :
type ServoCommand struct {
	Pins         []rpio.Pin
	neutralDuty  int
	rootModifier int
}

// PrintAllPins :
func (sc *ServoCommand) PrintAllPins() {
	for i, pin := range sc.Pins {
		fmt.Printf("Pin(%v): %v\n", i, pin.Read())
	}

}

// SwitchAllLow :
func (sc *ServoCommand) SwitchAllLow() {
	sc.Pins[0].DutyCycle(0, 1000)
	for _, pin := range sc.Pins {
		pin.Write(rpio.Low)
	}
}

// TurnCW :
func (sc *ServoCommand) TurnCW(duty int) {
	realDuty := sc.neutralDuty + int(math.Abs(float64(duty))) - 20
	sc.Pins[0].DutyCycle(uint32(realDuty), 1000)
	time.Sleep(time.Second / 50)
	//sc.PrintAllPins()
}

// TurnCCW :
func (sc *ServoCommand) TurnCCW(duty int) {
	realDuty := sc.neutralDuty - int(math.Abs(float64(duty))) + 20
	sc.Pins[0].DutyCycle(uint32(realDuty), 1000)
	time.Sleep(time.Second / 50)
	//sc.PrintAllPins()
}

// WaveDrive :
func (sc *ServoCommand) WaveDrive() {
	sc.Pins[0].DutyCycle(uint32(sc.neutralDuty), 1000)
	time.Sleep(time.Second / 50)
}

// FullStep :
func (sc *ServoCommand) FullStep() {
	sc.SwitchAllLow()
}

// NewServoCommand :
func NewServoCommand(pinNumbers []int, neutralDuty, rootModifier int) *ServoCommand {
	var pins []rpio.Pin
	fmt.Println("Pins", pinNumbers)

	pin := rpio.Pin(pinNumbers[0])
	pin.Mode(rpio.Pwm)
	pin.Freq(50000)
	pin.DutyCycle(uint32(neutralDuty), 1000)

	fmt.Println("Appending pin", pin)
	pins = append(pins, pin)

	command := &ServoCommand{
		Pins:         pins,
		neutralDuty:  neutralDuty,
		rootModifier: rootModifier,
	}

	return command
}
