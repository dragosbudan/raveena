package input

// KeyHandler :
type KeyHandler struct {
	Msgs           chan Data
	InputProcessor IProcessor
}

// Scan :
func (ic *KeyHandler) Scan(msg Data) {
	if ic.Msgs != nil {
		ic.Msgs <- msg
	}
}

// Consume :
func (ic *KeyHandler) Consume() {
	if ic.Msgs != nil {
		for {
			msg := <-ic.Msgs
			if ic.InputProcessor != nil {
				ic.InputProcessor.Process(&msg)
			}
		}
	}
}

// NewKeyInputHandler :
func NewKeyInputHandler(inputProc IProcessor) *KeyHandler {
	kih := &KeyHandler{
		Msgs:           make(chan Data),
		InputProcessor: inputProc,
	}
	return kih
}
