package command

import (
	"fmt"
	"time"
)

// FakeCommand :
type FakeCommand struct {
}

// PrintAllPins :
func (sc *FakeCommand) PrintAllPins() {
	time.Sleep(1 * time.Second)
	fmt.Println("FakeCommand")
}

// SwitchAllLow :
func (sc *FakeCommand) SwitchAllLow() {
	fmt.Println("Fake Command: SwitchAllLow")
	time.Sleep(1 * time.Second)
}

// TurnCW :
func (sc *FakeCommand) TurnCW(duty int) {
	time.Sleep(1 * time.Second)
	fmt.Println("Fake Command: TurnCW")
}

// TurnCCW :
func (sc *FakeCommand) TurnCCW(duty int) {
	time.Sleep(1 * time.Second)
	fmt.Println("Fake Command: TurnCCW")
}

// WaveDrive :
func (sc *FakeCommand) WaveDrive() {
	time.Sleep(1 * time.Second)
	fmt.Println("Fake Command: WaveDrive")
}

// FullStep :
func (sc *FakeCommand) FullStep() {
	time.Sleep(1 * time.Second)
	fmt.Println("Fake Command: FullStep")
}

// NewFakeCommand :
func NewFakeCommand() *FakeCommand {
	return &FakeCommand{}
}
