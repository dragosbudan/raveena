package input

import (
	"fmt"
	"raveena/controller"
)

// Processor :
type Processor struct {
	Controllers []controller.IController
}

// Process :
func (kip *Processor) Process(data *Data) error {
	var err error
	if kip.Controllers != nil && len(kip.Controllers) != 0 {
		err = fmt.Errorf("No Controllers in Processor")
		if data == nil {
			err = fmt.Errorf("Input data is nil")
		} else {
			for _, ctrl := range kip.Controllers {
				switch ctrl.GetType() {
				case controller.DC:
					ctrl.ChangeDuty(data.DcMotor)
					break
				case controller.SERVO:
					ctrl.ChangeDuty(data.Servo)
					break
				case controller.STEP:
					ctrl.ChangeDuty(0)
					break
				}
				ctrl.ChangeState(data.State)
			}

		}
	}
	return err
}

// NewInputProcessor :
func NewInputProcessor(controllers []controller.IController) *Processor {
	kip := &Processor{
		Controllers: controllers,
	}
	return kip
}
