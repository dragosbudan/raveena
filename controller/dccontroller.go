package controller

import (
	"fmt"
	"raveena/command"
)

// DcController :
type DcController struct {
	Duty    int
	State   State
	Command command.IGpioCommand
	Type    Type
}

// Run : Dc Control
func (dc *DcController) Run() {
loop:
	for {
		switch dc.State {
		case RUNNING:
			if dc.Duty > 0 {
				dc.Command.TurnCW(dc.Duty)
			} else if dc.Duty < 0 {
				dc.Command.TurnCCW(dc.Duty)
			} else {
				dc.Command.SwitchAllLow()
			}
		case PAUSED:
			dc.Command.SwitchAllLow()
		case STOPPED:
			dc.Command.SwitchAllLow()
			break loop
		}
	}
}

// ChangeState :
func (dc *DcController) ChangeState(state State) {
	if dc.State != state {
		fmt.Println("Change State:", state)
		dc.State = state
	}
}

// ChangeDuty :
func (dc *DcController) ChangeDuty(duty int) {
	dc.Duty = duty
}

// GetType :
func (dc *DcController) GetType() Type {
	return dc.Type
}

// CleanUp : Dc Control
func (dc *DcController) CleanUp() {
	dc.State = STOPPED
	dc.Command.SwitchAllLow()
}

// NewDcController :
func NewDcController(state State, command command.IGpioCommand) DcController {
	dcCtlr := &DcController{
		State:   state,
		Command: command,
		Type:    DC,
		Duty:    0,
	}

	return *dcCtlr
}
