package cleanup

import (
	"fmt"
	"reflect"

	"github.com/stianeikeland/go-rpio"
)

// SystemCleanupHandler :
type SystemCleanupHandler struct {
	Disposables []IDisposable
}

// Invoke :
func (sc *SystemCleanupHandler) Invoke() {
	fmt.Println("Performing clean up...")
	for _, disp := range sc.Disposables {
		fmt.Println(reflect.TypeOf(disp), "cleaning up...")
		disp.CleanUp()
	}

	err := rpio.Close()
	if err != nil {
		fmt.Println("rpio.Close():", err)
	}
}

// NewCleanUpHandler :
func NewCleanUpHandler(disposables []IDisposable) *SystemCleanupHandler {
	sch := &SystemCleanupHandler{
		Disposables: disposables,
	}
	return sch
}
