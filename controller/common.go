package controller

// IController :
type IController interface {
	Run()
	ChangeDuty(duty int)
	ChangeState(state State)
	GetType() Type
}

// Type :
type Type int

// Exported Types :
const (
	SERVO Type = 0
	DC    Type = 1
	STEP  Type = 2
)

// State :
type State int

// Exported States :
const (
	STOPPED State = 0
	RUNNING State = 1
	PAUSED  State = 2
)
