package main

import (
	"fmt"
	"raveena/cleanup"
	"raveena/command"
	"raveena/controller"
	"raveena/input"
	"raveena/sysserv"
	"sync"

	"github.com/bt/btioc"
	"github.com/splace/joysticks"
	rpio "github.com/stianeikeland/go-rpio"
)

func btiocRetrieve(name string) interface{} {
	res, err := btioc.Retrieve(name)

	if err != nil {
		panic(err)
	}

	return res
}

func main() {

	controllers := []controller.IController{}
	disposables := []cleanup.IDisposable{}

	err := rpio.Open()

	if err == nil {
		btioc.Register("ServoCommand", command.NewServoCommand([]int{13}, 88, 0))
		btioc.Register("DcCommand", command.NewDcCommand([]int{18, 23, 24, 25}))
	} else {
		btioc.Register("ServoCommand", &command.FakeCommand{})
		btioc.Register("DcCommand", &command.FakeCommand{})
	}

	servoComm := btiocRetrieve("ServoCommand").(command.IGpioCommand)
	dcComm := btiocRetrieve("DcCommand").(command.IGpioCommand)

	servo := controller.NewServoController(controller.RUNNING, servoComm)
	controllers = append(controllers, &servo)
	disposables = append(disposables, &servo)
	dc := controller.NewDcController(controller.RUNNING, dcComm)
	controllers = append(controllers, &dc)
	disposables = append(disposables, &dc)

	if joysticks.DeviceExists(1) {
		btioc.Register("Input", input.NewGamepadInputFactory(controllers))
	} else {
		btioc.Register("Input", input.NewKeyInputFactory(controllers))
	}

	cup := cleanup.NewCleanUpHandler(disposables)

	defer func() {
		cup.Invoke()
	}()

	si := sysserv.SysInterrupt{
		CleanupHandler: cup,
	}

	si.WaitForKeyInterrupt()

	i := btiocRetrieve("Input").(input.IInput)
	go i.WaitForInput()

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		dc.Run()
		defer wg.Done()
	}()

	go func() {
		servo.Run()
		defer wg.Done()
	}()

	wg.Wait()

	fmt.Println("Finished")
}
