package wrapper

import (
	rpio "github.com/stianeikeland/go-rpio"
)

// IPin :
type IPin interface {
	Create(pinNumber int) bool
	InitPwm(freq int, startDutyLen, cycleLen uint32)
	InitOutput()
	Read()
	Write(duty uint32)
}

// Pin :
type Pin struct {
	pinRaw         rpio.Pin
	pinMode        rpio.Mode
	pwmCycleLength uint32
}

// Create :
func Create(number int) rpio.Pin {
	// /pw.pinRaw = rpio.Pin(number)

	// pw.pinRaw.Output()
	// pw.pinRaw.High()
	// defer pw.pinRaw.Low()

	// if pw.pinRaw.Read() != 1 {
	// 	panic(fmt.Errorf("Failed to create pin: %d", number))
	// }

	return rpio.Pin(number)
}

// InitPwm :
func (pw *Pin) InitPwm(freq int, startDutyLen, cycleLen uint32) {
	pw.pinRaw.Pwm()
	pw.pinMode = rpio.Pwm
	pw.pwmCycleLength = cycleLen
	pw.pinRaw.Freq(freq * int(cycleLen))
	pw.pinRaw.DutyCycle(startDutyLen, cycleLen)
}

// InitOutput :
func (pw *Pin) InitOutput() {
	pw.pinRaw.Output()
	pw.pinMode = rpio.Output
	pw.pinRaw.Write(rpio.Low)
}

// Write:
func (pw *Pin) Write(duty uint32) {
	switch pw.pinMode {
	case rpio.Output:
		pw.pinRaw.Write(rpio.State(duty))
	case rpio.Pwm:
		pw.pinRaw.DutyCycle(duty, pw.pwmCycleLength)
	}
}

// Read:
func (pw *Pin) Read() uint8 {
	var r uint8
	switch pw.pinRaw.Read() {
	case rpio.Low:
		r = 0
	case rpio.High:
		r = 1
	}
	return r
}
