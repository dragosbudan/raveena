package input

import "raveena/controller"

// Data : Input mapped to controller functionality
type Data struct {
	Servo   int
	DcMotor int
	State   controller.State
}

// IProcessor :
type IProcessor interface {
	Process(data *Data) error
}

// IHandler : handles input
type IHandler interface {
	Scan(msg Data)
	Consume()
}

// IInput :
type IInput interface {
	WaitForInput()
}
