package cleanup

// IDisposable :
type IDisposable interface {
	CleanUp()
}

// ICleanupHandler :
type ICleanupHandler interface {
	Invoke()
}
