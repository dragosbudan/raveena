package tests

import (
	"raveena/input"
	"raveena/mocks"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
)

func Test_should_trigger_consume_after_a_scan(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockInputProc := mocks.NewMockIProcessor(mockCtrl)

	sut := input.NewKeyInputHandler(mockInputProc)
	inputData := input.Data{}
	go sut.Scan(inputData)

	mockInputProc.EXPECT().Process(&inputData).Times(1)

	go sut.Consume()
	time.Sleep(1 * time.Second)
}
