package sysserv

import (
	"fmt"
	"os"
	"os/signal"
	"raveena/cleanup"
	"reflect"
)

// SysInterrupt : service for system interrupt
type SysInterrupt struct {
	CleanupHandler cleanup.ICleanupHandler
}

// WaitForKeyInterrupt : keyboard interrupt
func (s *SysInterrupt) WaitForKeyInterrupt() {
	// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
	// Run cleanup when signal is received
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		<-signalChan
		if s.CleanupHandler != nil {
			fmt.Println("\nReceived an interrupt, invoking cleanup handler:", reflect.TypeOf(s.CleanupHandler))
			s.CleanupHandler.Invoke()
		}

		os.Exit(1)
	}()
}
