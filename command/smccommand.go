package command

import (
	"fmt"
	"time"

	rpio "github.com/stianeikeland/go-rpio"
)

// SmcCommand :
type SmcCommand struct {
	Pins []rpio.Pin
}

// PrintAllPins :
func (sc *SmcCommand) PrintAllPins() {
	for i, pin := range sc.Pins {
		fmt.Printf("Pin(%v): %v\n", i, pin.Read())
	}

}

// SwitchAllLow :
func (sc *SmcCommand) SwitchAllLow() {
	for _, pin := range sc.Pins {
		pin.Write(rpio.Low)
	}
}

// TurnCW :
func (sc *SmcCommand) TurnCW(duty int) {
	// step
	for i := 0; i < len(sc.Pins); i++ {

		//fmt.Println("i: ", i)

		sc.SwitchAllLow()

		sc.Pins[i].Write(rpio.High)
		//sc.PrintAllPins()

		time.Sleep(5 * time.Millisecond)
	}
}

// TurnCCW :
func (sc *SmcCommand) TurnCCW(duty int) {
	// step
	for i := 3; i >= 0; i-- {

		sc.SwitchAllLow()

		sc.Pins[i].Write(rpio.High)

		time.Sleep(5 * time.Millisecond)
	}
}

// WaveDrive :
func (sc *SmcCommand) WaveDrive() {
	// step
	for i := 0; i < len(sc.Pins); i++ {

		//fmt.Println("i: ", i)

		sc.SwitchAllLow()

		sc.Pins[i].Write(rpio.High)

		//printAllPins(smc.Pins)
		time.Sleep(2 * time.Millisecond)
	}
}

// FullStep :
func (sc *SmcCommand) FullStep() {
	for i := 0; i < len(sc.Pins); i++ {

		//fmt.Println("i: ", i)

		sc.SwitchAllLow()

		sc.Pins[i].Write(rpio.High)
		if i+1 < len(sc.Pins) {
			sc.Pins[i+1].Write(rpio.High)
		} else {
			sc.Pins[len(sc.Pins)-(i+1)].Write(rpio.High)
		}

		//printAllPins(smc.Pins)
		time.Sleep(100 * time.Millisecond)
	}
}

// NewSmcCommand :
func NewSmcCommand(pinNumbers []int) *SmcCommand {
	var pins []rpio.Pin
	fmt.Println("Pins", pinNumbers)

	for i := 0; i < len(pinNumbers); i++ {
		pin := rpio.Pin(pinNumbers[i])
		pin.Mode(rpio.Output)
		fmt.Println("Appending pin", pin)
		pins = append(pins, pin)
	}

	command := &SmcCommand{
		Pins: pins,
	}

	return command
}
