package controller

import (
	"fmt"
	"raveena/command"
)

// ServoController :
type ServoController struct {
	Duty    int
	State   State
	Command command.IGpioCommand
	Type    Type
}

// Run : sc Control
func (sc *ServoController) Run() {
loop:
	for {
		switch sc.State {
		case RUNNING:
			if sc.Duty > 0 {
				sc.Command.TurnCW(sc.Duty)
			} else if sc.Duty < 0 {
				sc.Command.TurnCCW(sc.Duty)
			} else {
				sc.Command.WaveDrive()
			}
		case PAUSED:
			sc.Command.SwitchAllLow()
		case STOPPED:
			sc.Command.SwitchAllLow()
			break loop
		}
	}
}

// ChangeState :
func (sc *ServoController) ChangeState(state State) {
	if sc.State != state {
		fmt.Println("Change State:", state)
		sc.State = state
	}
}

// ChangeDuty :
func (sc *ServoController) ChangeDuty(duty int) {
	sc.Duty = duty
}

// GetType :
func (sc *ServoController) GetType() Type {
	return sc.Type
}

// CleanUp : sc Control
func (sc *ServoController) CleanUp() {
	sc.State = STOPPED
	sc.Command.SwitchAllLow()
}

// NewServoController :
func NewServoController(state State, command command.IGpioCommand) ServoController {
	smcCtlr := &ServoController{
		State:   state,
		Command: command,
		Type:    SERVO,
		Duty:    0,
	}

	return *smcCtlr
}
